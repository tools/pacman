module.exports = (w) => {
    return {
        files: [
            "src/**/*.ts"
        ],
        tests: [
            "spec/**.spec.ts"
        ],
        compilers: {
            "**/*.ts": w.compilers.typeScript({module: "commonjs"}),
        },
        debug: true,
        env: {
            type: "node"
        },
        testFramework: "mocha",
        setup: function (wallaby) {
            require("bluebird").config({
                longStackTraces: true
            })
        }
    }
}