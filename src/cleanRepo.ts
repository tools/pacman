import { readLocalRepo, writeLocalRepo, getInstalledPackages } from './localRepo';
import * as _ from 'lodash';
import { Package, packageKey, Dependency } from './interfaces';
import { solveDependencies } from './solveDependencies';
import { warn } from './log';
import { native } from './native';
import { getPackageRootDirectory } from './packageDirectory';
import { filterObject } from './utils/functools';
import { packagesBeingUsed } from './packagesBeingUsed';

/**
 * Delete all packages that are not transitive dependencies of the local
 * repository root dependencies.
 *
 * @param butKeepThesePackages If some packages are specified here, they will
 * not be deleted. Packages specified here may or may not exist in the local
 * repository.
 */
export function cleanRepo(butKeepThesePackages: Package[] = []): void {
  const localRepo = readLocalRepo();

  // This is a Mark-and-sweep algorithm
  // http://www.brpreiss.com/books/opus5/html/page424.html

  // A set is built with all the dependencies of all the installed games
  const installedPackages = getInstalledPackages();
  let usedPackages = _.reduce(
    localRepo.rootDependencies,
    (prev: Package[], dep: Dependency) => {
      const packages = solveDependencies([dep], installedPackages);
      if (packages != null) return _.unionBy(prev, packages, packageKey);
      // SHOULD not happen.
      // It would mean the game was marked as installed but no package set
      // satisfies it.
      warn('found orphan dependency whilst cleaning the repo');
      return prev;
    },
    <Package[]>[],
  );

  // If a game is being played now, it should not be deleted.
  usedPackages = _.unionBy(usedPackages, packagesBeingUsed, packageKey);

  const unusedPackages = _.differenceBy(
    localRepo.installedPackages, usedPackages, packageKey);

  const packagesToRemove = _.differenceBy(
    unusedPackages, butKeepThesePackages, packageKey);

  packagesToRemove.forEach((pkg: Package) => {
    native.recursiveDelete(getPackageRootDirectory(pkg));
  });

  // Update local.json

  // We use localRepo.installedPackages instead of getInstalledPackages()
  // because we don't want the container-sdk there.
  const packagesRemaining = _.differenceBy(
    localRepo.installedPackages, packagesToRemove, packageKey);
  const packageRemainingKeys = packagesRemaining.map(packageKey);

  // Update the local repo
  writeLocalRepo({
    installedPackages: packagesRemaining,
    rootDependencies: localRepo.rootDependencies,
    installedSizes: filterObject(localRepo.installedSizes, key =>
      packageRemainingKeys.indexOf(key) !== -1),
  });
}
