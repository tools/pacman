import { HttpResponse, DownloadProgress } from './interfaces';
import { ensure, AssertionError } from './utils/assert';
import { HttpError, ConnectionError, NotEnoughSpace } from './errors';
import * as Promise from 'bluebird';
import { native } from './native';

export function httpGetText(url: string): Promise<HttpResponse> {
  return new Promise<HttpResponse>((resolve, reject) => {
    return native.httpGetText(url, (err, text, code) => {
      if (err) {
        reject(new ConnectionError());
      } else {
        resolve({ code: ensure(code), text: ensure(text) });
      }
    });
  });
}

export function httpGetJson<T>(url: string): Promise<T> {
  return httpGetText(url)
    .then((res) => {
      if (res.code === 200) {
        return <T>JSON.parse(res.text);
      }
      throw new HttpError(res.code, res.text);
    });
}

/**
 * Downloads and uncompresses a zip package in a given directory.
 *
 * Promise wrapper over native.installZip()
 */
export function installZip(
  url: string,
  path: string,
  cbProgress: (p: DownloadProgress) => void,
): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    native.installZip(url, path, cbProgress, (err, code) => {
      if (err === 'network-error') {
        reject(new ConnectionError());
      } else if (err === 'http-error') {
        if (code == null) {
          throw new AssertionError('native api returned \'http-error\' on' +
            ' installZip() but did not specify http error code.');
        }
        reject(new HttpError(code, `error ${code} in installZip(${url})`));
      } else if (err === 'disk-full') {
        reject(new NotEnoughSpace());
      } else {
        resolve();
      }
    });
  });
}
