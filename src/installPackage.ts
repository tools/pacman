import { RemotePackageList, getPackageDownloadSize } from './packageCache';
import { readLocalRepo, writeLocalRepo, getInstalledPackages } from './localRepo';
import { mapObject } from './utils/functools';
import { RuntimeError } from './errors';
import { Package, DownloadProgress, packageKey, Dependency } from './interfaces';
import { solveDependencies } from './solveDependencies';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import { getPackageUrl } from './remoteRepo';
import { installZip } from './net';
import { getPackageRootDirectory } from './packageDirectory';
import { cleanRepo } from './cleanRepo';
import { freeSpaceForPackages } from './lru';
import { ensure } from './utils/assert';
import { getContainerPackage } from './containerVersion';

/**
 * Install or update package.
 *
 * @param packageName
 * @param remotePackageList
 * @param cbProgress Will be called as the required packages are downloaded.
 */
export function installPackageByName(
  packageName: string,
  remotePackageList: RemotePackageList,
  cbProgress?: (p: DownloadProgress) => void)
: Promise<Package> {
  // Choose the best package and dependencies from the remote repository
  const packagesNeeded = solveDependencies(
    // any version of the requested package (preferably the most recent one)
    [{ name: packageName, minVersion: null, maxVersion: null }],
    [getContainerPackage()].concat(remotePackageList.packages),
  );
  if (packagesNeeded == null) {
    throw new RuntimeError('could not find suitable dependencies for the requested package');
  }

  // Filter out those already installed
  const packagesMissing = _.differenceBy(
    packagesNeeded, getInstalledPackages(), packageKey);

  return freeSpaceForPackages(packagesMissing, packagesNeeded, remotePackageList)
    .then(() =>
      installPackageSetInDisk(packagesMissing, remotePackageList, cbProgress))
    .then(() => {
      // Update the local repository
      const localRepo = readLocalRepo();

      const rootDependencies = _.unionBy(
        localRepo.rootDependencies,
        [{ name: packageName, minVersion: null, maxVersion: null }],
        (dep: Dependency) => dep.name,
      );
      const installedPackages = localRepo.installedPackages.concat(packagesMissing);
      const installedSizes = mapObject(installedPackages, p =>
        [packageKey(p), ensure(remotePackageList.packageSizes[packageKey(p)]).disk]);

      writeLocalRepo({
        rootDependencies,
        installedPackages,
        installedSizes,
      });

      // Delete orphans in case we've accidentally updated shared dependencies.
      cleanRepo();

      return _.find(packagesNeeded, (p: Package) => p.name === packageName)!;
    });
}

/**
 * Downloads and uncompresses a set of packages from the repository.
 *
 * Does NOT modify the local repo. That's up to the caller.
 */
function installPackageSetInDisk(
    packages: Package[],
    remotePackageList: RemotePackageList,
    cbProgress?: (p: DownloadProgress) => void)
: Promise<void> {
  const totalDownloadSize = _.sumBy(
    packages, (p: Package) => getPackageDownloadSize(p, remotePackageList));
  // Send an initial progress notification with the total size
  if (cbProgress) cbProgress({ downloadedSize: 0, totalSize: totalDownloadSize });

  let sizeOfPackagesAlreadyInstalled = 0;

  // Install each of the missing packages
  return Promise.each(packages, (pkg: Package) =>
      installZip(getPackageUrl(pkg), getPackageRootDirectory(pkg), (packageProgress) => {
        if (cbProgress) {
          cbProgress({
            downloadedSize: sizeOfPackagesAlreadyInstalled + packageProgress.downloadedSize,
            totalSize: totalDownloadSize,
          });
        }
      }).then(() => {
        sizeOfPackagesAlreadyInstalled += getPackageDownloadSize(pkg, remotePackageList);
      }),
  ).thenReturn();
}
