import { Package, Dependency } from './interfaces';
import { compareTuples } from './utils/compareTuples';

function find<T>(list: T[], predicate: (t: T) => boolean): T | undefined {
  for (let i = 0; i < list.length; i += 1) {
    if (predicate(list[i])) {
      return list[i];
    }
  }
}

export function versionInRange(
  supplied: number[],
  min: number[] | null,
  max: number[] | null,
): boolean {
  // min <= supplied < max
  return (min == null || compareTuples(min, supplied) <= 0) &&
      (max == null || compareTuples(supplied, max) < 0);
}

export function moreRecentPackagesFirst(a: Package, b: Package): number {
  return compareTuples(b.version, a.version);
}

/**
 * The local repository may have several options for a certain
 * dependency of a package. Only one package must be used for
 * each game dependency, and they must be selected every time
 * the game is launched.
 *
 * This function receives a package (e.g. a game) and returns a
 * list of chosen dependencies that match its requirements
 * and are as updated as possible.
 *
 * This function may also be used on remote package lists to
 * resolve package dependencies on install time.
 */
export function solveDependencies(
  dependencies: Dependency[],
  packageList: Package[],
): Package[] | null {
  return recursiveSolve(dependencies, []);

  /**
   * Backtracking algorithm to find packages that satisfy some dependencies
   * without conflicts with already selected packages.
   *
   * @param dependencies Dependencies of the package to install.
   * @param currentSolution List of packages already selected. Only one
   * version per package may become part of the solution.
   */
  function recursiveSolve(
    dependencies: Dependency[],
    currentSolution: Package[],
  ): Package[] | null {
    if (dependencies.length === 0) {
      // No more dependencies to solve for this package.
      return currentSolution;
    }
    // Solve first dependency.
    const dep = dependencies[0];

    // Is there a package already providing it in the current solution?
    const currentPackage = find(currentSolution, p => p.name === dep.name);
    // If there is such a package but its version conflicts, this is not a valid solution.
    if (currentPackage && !versionInRange(currentPackage.version, dep.minVersion, dep.maxVersion)) {
      return null;
    }

    const versionsFulfilling =
      // If there is already a package selected for this dependency name, we can't
      // choose another.
      currentPackage != null ? [currentPackage]
        // If there is currently not such a package in the solution, let's find one.
        // Try with the most updated packages first.
        : packageList
          .filter(p => p.name === dep.name &&
            versionInRange(p.version, dep.minVersion, dep.maxVersion))
          .sort(moreRecentPackagesFirst);

    // Try each of the versions found until one is a solution
    for (const candidateDependency of versionsFulfilling) {
      // Solve nested dependencies of the selected package
      const newPartialSolution = recursiveSolve(
        candidateDependency.dependencies,
        currentSolution.concat([candidateDependency]),
      );

      // This dependency can be satisfied with this version. All its nested
      // dependencies match. They're now in `newPartialSolution`.
      if (newPartialSolution != null) {
        // Solve the rest of dependencies of `package`.
        const solution = recursiveSolve(dependencies.slice(1), newPartialSolution);

        if (solution != null) {
          return solution;
        }
      }
      // Else keep trying with other version of the package.
    }

    // Tried every version. No solution.
    return null;
  }
}
