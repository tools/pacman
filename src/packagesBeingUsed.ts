import { Package } from './interfaces';
import { getInstalledPackages } from './localRepo';
import { solveDependencies } from './solveDependencies';
import { cleanRepo } from './cleanRepo';

/**
 * Packages of the game being played currently, if any. Both the game and the
 * dependencies are included.
 *
 * A game will not be deleted while it's being played.
 */
export let packagesBeingUsed: Package[] = [];

export function startGame(gameName: string): Package[] | null {
  const dependencies = solveDependencies(
    [{ name: gameName, minVersion: null, maxVersion: null }],
    getInstalledPackages(),
  );

  if (dependencies != null) {
    packagesBeingUsed = dependencies;
  }

  return dependencies;
}

export function stopGame() {
  packagesBeingUsed = [];

  // If a cleaning operation left the packages being used even though
  // they are not reachable from the roots, remove them now
  // (otherwise this will be no-op).
  cleanRepo();
}
