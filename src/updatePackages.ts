import { readLocalRepo } from './localRepo';
import * as _ from 'lodash';
import { fetchRemotePackageList } from './packageCache';
import { installPackageByName } from './installPackage';
import * as Promise from 'bluebird';
import { enqueueTask, TaskPriority } from './taskQueue';
import { readLruList } from './lru';
import { NotEnoughSpace } from './errors';
import noop = require('lodash/noop');

function updateGameIfStillExists(gameName: string): Promise<void> {
  const localRepo = readLocalRepo();

  if (!_.find(localRepo.installedPackages, p => p.name === gameName)) {
    // No game anymore, nothing to update, we're done!
    return Promise.resolve();
  }

  return fetchRemotePackageList()
    .then(remotePackageList => installPackageByName(gameName, remotePackageList, () => {}))
    .thenReturn();
}

export function updateAllPackages(): Promise<void> {
  const localRepo = readLocalRepo();

  // Update one game each time.
  //
  // This way we don't store at the same time more than two versions of any
  // given game, reducing the chances for an out of memory and needing to
  // uninstall games to make free space.
  //
  // Since there are chances we end up without space anyway, we will try
  // to update most recently played games first.
  const lru = readLruList();
  const gamesRecentFirst = _.sortBy(localRepo.rootDependencies, (dep) => {
    const lruPosition = lru.indexOf(dep.name);
    if (lruPosition !== -1) {
      return lruPosition;
    }
    // Games never played yet must be the last ones to be updated!
    return Infinity;
  });

  const updatePromises = _.map(gamesRecentFirst, dep =>
    enqueueTask(() => updateGameIfStillExists(dep.name), TaskPriority.Low));

  // We've finished when all the update tasks are complete
  return Promise.all(updatePromises).thenReturn();
}
