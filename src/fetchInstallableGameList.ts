import { solveDependencies } from './solveDependencies';
import { fetchRemotePackageList } from './packageCache';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import { getContainerPackage } from './containerVersion';

export function fetchInstallableGameList(): Promise<string[]> {
  return fetchRemotePackageList()
    .then((remotePackageList) => {
      const availablePackages = [getContainerPackage()]
        .concat(remotePackageList.packages);

      return _(remotePackageList.packages)
        .filter(p => p.role === 'game')
        .map(p => p.name)
        .uniq()
        .filter(gameName =>
          solveDependencies(
            [{ name: gameName, minVersion: null, maxVersion: null }],
            availablePackages,
          ) != null)
        .value();
    });
}
