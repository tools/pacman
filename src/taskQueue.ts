import { assert } from './utils/assert';
import * as Promise from 'bluebird';
import * as _ from 'lodash';

export enum TaskPriority {
  // Install
  Normal,
  // Update
  Low,
}

interface TaskQueueItem<T> {
  priority: TaskPriority;
  run: () => Promise<T>;
  deferred: {
    resolve: (v: T) => void
    reject: (e: any) => void,
  };
}

class PacmanScheduler {
  /** Currently running task. */
  private currentTask: TaskQueueItem<any> | null = null;

  /** Task scheduled to run after `currentTask`, FIFO with priorities. */
  private pendingTasks: TaskQueueItem<any>[] = [];

  enqueueTask<T>(runnable: () => Promise<T>, priority: TaskPriority): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      const task: TaskQueueItem<T> = {
        priority,
        run: runnable,
        deferred: {
          resolve,
          reject,
        },
      };

      // FIFO
      this.pendingTasks.push(task);

      if (this.currentTask == null) {
        // Start running the first and only task, which should be this one
        assert(this.pendingTasks.length === 1, 'pendingTasks.length == 1');
        this.runTask(this.popTask()!);
      }
    });
  }

  private popTask(): TaskQueueItem<any> | null {
    const normalPriority = _.find(this.pendingTasks, t => t.priority === TaskPriority.Normal);
    const lowPriority = _.find(this.pendingTasks, t => t.priority === TaskPriority.Low);

    const task = normalPriority || lowPriority;
    if (task != null) {
      // Remove from the queue
      _.remove(this.pendingTasks, t => t === task);
    }
    return task || null;
  }

  private runTask<T>(task: TaskQueueItem<T>) {
    task.run()
      .then((result) => {
        task.deferred.resolve(result);
      })
      .catch((error) => {
        task.deferred.reject(error);
      })
      .finally(() => {
        // Mark the task as done
        this.currentTask = null;
        // While the queue is not empty, run the next task
        const nextTask = this.popTask();
        if (nextTask != null) {
          return this.runTask(nextTask);
        }
      });
  }
}
const scheduler = new PacmanScheduler();


export function enqueueTask<T>(runnable: () => Promise<T>, priority: TaskPriority): Promise<T> {
  return scheduler.enqueueTask(runnable, priority);
}
