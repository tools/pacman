import { AssertionError } from './utils/assert';

export function info(message: string) {
  console.log(message);
}

export function error(message: string) {
  // During development these are errors
  if (typeof describe !== 'undefined') throw new AssertionError(`ERROR: ${message}`);
  console.error(message);
}

export function warn(message: string) {
  // During development these are errors
  if (typeof describe !== 'undefined') throw new AssertionError(`WARN: ${message}`);
  console.error(message);
}
