export interface Dependency {
  name: string;
  minVersion: number[] | null;
  maxVersion: number[] | null;
}

export class Package {
  name: string;
  version: number[];
  dependencies: Dependency[];
  role: 'game' | 'framework' | 'client-sdk' | 'container-sdk';
}

export interface DownloadProgress {
  totalSize: number;
  downloadedSize: number;
}

export interface HttpResponse {
  code: number;
  text: string;
}

/**
 * Used to uniquely identify a package by its name and version.
 */
export function packageKey(pkg: Package) {
  return `${pkg.name}-${pkg.version.join('.')}`;
}
