import { Package } from './interfaces';

/**
 * Read by browserify (with envify)... or node.
 * https://github.com/hughsk/envify
 */
declare module process {
  export const env: any;
}

export let repoUrl = process.env.REPO_URL || 'https://psicool.com/api/repo';

export function setRepoUrl(url: string) {
  repoUrl = url;
}

export function getPackageUrl(pkg: Package): string {
  return `${repoUrl}/${pkg.role}/${pkg.name}/${pkg.name}-${pkg.version.join('.')}.zip`;
}
