import { Package } from './interfaces';

export function getPackageRootDirectory(pkg: Package) {
  return `${pkg.role}/${pkg.name}/${pkg.name}-${pkg.version.join('.')}`;
}
