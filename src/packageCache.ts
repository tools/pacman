import { Package } from './interfaces';
import { httpGetJson } from './net';
import * as Promise from 'bluebird';
import { repoUrl } from './remoteRepo';
import { ensure } from './utils/assert';

export interface RemotePackageList {
  packages: Package[];
  packageSizes: {
    [packageNameAndVersion: string]: {
      /** Size as bytes when compressed */
      download: number
      /** Size as bytes when uncompressed in 8 KB blocks */
      disk: number,
    },
  };
}

export function getPackageDownloadSize(pkg: Package, remotePackageList: RemotePackageList): number {
  if (pkg.role !== 'container-sdk') {
    return ensure(remotePackageList.packageSizes[`${pkg.name}-${pkg.version.join('.')}`]).download;
  }
  // The container SDK is not defined in the remote package list and occupies
  // no size.
  return 0;
}

export function getPackageInstalledSize(
  pkg: Package,
  remotePackageList: RemotePackageList,
): number {
  if (pkg.role !== 'container-sdk') {
    return ensure(remotePackageList.packageSizes[`${pkg.name}-${pkg.version.join('.')}`]).disk;
  }
  // The container SDK is not defined in the remote package list and occupies
  // no size.
  return 0;
}

export interface PackageCache {
  list: RemotePackageList;
  downloaded: Date;
}

const packageCache: PackageCache | null = null;

/**
 * Returns the cached version of the remote package list immediately.
 *
 * If the list is not cached (it has never been downloaded) it will return null
 */
// export function getRemotePackageList(): PackageCache | null {
//     const contents = native.fileReadSync("package-cache.json");
//     if (contents) {
//         return (<PackageCache>JSON.parse(contents)).list;
//     } else {
//         return null;
//     }
// }

// /**
//  * Returns immediately the cached version of the remote package list if it's
//  * recent enough, otherwise downloads a fresh one first.
//  */
/**
 * Fetches the repository package index.
 */
export function fetchRemotePackageList(): Promise<RemotePackageList> {
  return httpGetJson<RemotePackageList>(repoUrl + '/list.json');
  //
  // // If there is no cached list or it's older than 1 hour
  // if (!packageCache || new Date().getTime() - packageCache.downloaded.getTime() > 3600000) {
  //     // Fetch a new version
  //
  //         .then(function(list) {
  //             packageCache = {
  //                 list: list,
  //                 downloaded: new Date()
  //             }
  //         })
  // } else {
  //     // Otherwise keep using the same one
  //     return Promise.resolve<void>(undefined);
  // }
}
