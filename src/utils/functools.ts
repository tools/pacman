import * as Collections from 'typescript-collections';

export function map<V, Z>(iterable: V[], callback: (value: V, index: number) => Z): Z[] {
  const ret: Z[] = [];
  let i = 0;
  for (const value of iterable) {
    ret.push(callback(value, i));
    i += 1;
  }
  return ret;
}

export function entries<K, V>(dict: Collections.Dictionary<K, V>): [K, V][] {
  const ret: [K, V][] = [];
  dict.forEach((k, v) => {
    ret.push([k, v]);
  });
  return ret;
}

export function sum(iterable: number[]): number;
export function sum<V>(iterable: V[], callback: (value: V) => number): number;
export function sum<V>(iterable: V[], callback?: (value: any) => number): number {
  const f = callback || (v => v);
  let ret = 0;
  for (const value of iterable) {
    ret += f(value);
  }
  return ret;
}

export function groupBy<K, V>(list: V[], keyFn: (value: V) => K): Collections.Dictionary<K, V[]> {
  const ret = new Collections.Dictionary<K, V[]>();
  for (const value of list) {
    const key = keyFn(value);
    if (!ret.containsKey(key)) {
      ret.setValue(key, []);
    }
    ret.getValue(key)!.push(value);
  }
  return ret;
}

export function sortedByWith<T, V>(
  list: T[],
  byFn: (item: T) => V,
  cmpFn: (a: V, b: V) => number,
): T[] {
  const arrayCopy = (<T[]>[]).concat(list);
  return arrayCopy.sort((a, b) => cmpFn(byFn(a), byFn(b)));
}

export function minByWith<T, V>(
  list: T[],
  byFn: (item: T) => V,
  cmpFn: (a: V, b: V) => number,
): T | undefined {
  const ranking = sortedByWith(list, byFn, cmpFn);
  if (ranking.length > 0) return ranking[0];
}

export function maxByWith<T, V>(
  list: T[],
  byFn: (item: T) => V,
  cmpFn: (a: V, b: V) => number,
): T | undefined {
  const ranking = sortedByWith(list, byFn, cmpFn);
  if (ranking.length > 0) return ranking[ranking.length - 1];
}

export function nativeComparison<T>(a: T, b: T): number {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
}

export function subtractBy<T, V>(minuend: T[], subtrahend: T[], byFn: (item: T) => V): T[] {
  return subtractByWith(minuend, subtrahend, byFn, nativeComparison);
}

export function subtractByWith<T, V>(
  minuend: T[],
  subtrahend: T[],
  byFn: (item: T) => V,
  cmpFn: (a: V, b: V) => number,
): T[] {
  let ret: T[] = [];

  // Sort both sets
  const a = sortedByWith(minuend, byFn, cmpFn);
  const b = sortedByWith(subtrahend, byFn, cmpFn);

  // Iterate both sets, from lesser to greater
  let ai = 0;
  let bi = 0;
  while (true) {
    if (bi > b.length) {
      // All the remaining elemnts of a are greater than all the elements of b, and
      // therefore different.
      // Add all of them to the solution and end with a.
      ret = ret.concat(a.slice(ai));
      ai = a.length;
    }
    if (ai > a.length) {
      // We've explored a completely, no need to search more, no matter if elements
      // remain in b or not.
      return ret;
    }

    // Compare the current minuend element with the lowest subtrahend element
    const comp = cmpFn(byFn(a[ai]), byFn(b[bi]));
    if (comp === 0) { // a[ai] == b[bi]
      // the element is in both sets, skip it.
      ai += 1;
    } else if (comp > 0) { // a[ai] > b[bi]
      // try with the next element of b
      bi += 1;
    } else if (comp < 0) { // a[ai] < b[bi]
      // definitely we will not find the element in b, add it to the solution
      // and search the next element of b.
      ret.push(a[ai]);
      ai += 1;
    }
  }
}

export function filterObject<V>(obj: {[k: string]: V}, predicate: (k: string, v: V) => boolean)
: {[k: string]: V} {
  const ret: {[k: string]: V} = {};
  for (const key in obj) {
    if (predicate(key, obj[key])) {
      ret[key] = obj[key];
    }
  }
  return ret;
}

export function mapObject<V, T>(list: T[], predicate: (item: T) => [string, V]): {[k: string]: V};
export function mapObject<V, T>(list: T[], predicate: (item: T) => [number, V]): {[k: number]: V};
export function mapObject<V, T>(
  list: T[],
  predicate: (item: T) => [string | number, V],
): {[k: string]: V} {
  const ret: {[k: string]: V} = {};
  for (const item of list) {
    const [key, value] = predicate(item);
    ret[key] = value;
  }
  return ret;
}
