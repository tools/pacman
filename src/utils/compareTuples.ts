/**
 * Compares tuples item by item, in the same way Python does it.
 */
export function compareTuples(a: (number | string)[], b: (number | string)[]): number {
  if (a.length === 0 && b.length === 0) return 0; // Emptied both tuples, they are equal.
  if (a.length > 0 && b.length === 0) return 1; // Emptied b, a still has elements. a is greater.
  if (a.length === 0 && b.length > 0) return -1; // Emptied a, b still has elements. a is lesser.
  if (a[0] > b[0]) return 1; // a is greater
  if (a[0] < b[0]) return -1; // a is lesser
  return compareTuples(a.slice(1), b.slice(1)); // Compare rest of the tuple
}
