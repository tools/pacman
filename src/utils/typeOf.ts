export default function typeOf(value: any) {
  return (typeof value !== 'object' ? typeof value : value.constructor);
}

function isNullOrEmpty(thing: string | null): thing is string {
  return typeof thing === 'string' && thing.length > 0;
}

function test() {
  function useThing(thing: string | null) {
    if (isNullOrEmpty(thing)) {
      console.log(thing);
    } else {
      console.log(thing);
    }
  }
  useThing(null);
}
