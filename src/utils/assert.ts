import { RuntimeError } from '../errors';
import isInstance from './isInstance';
import typeOf from './typeOf';

export class AssertionError extends RuntimeError {
  constructor(message: string = 'Assertion error') {
    super(message);
  }
}

export interface PropertyDeclaration {
  name: string;
  type: any;
}

export function assert(value: boolean, message: string = 'Assertion failed') {
  if (!value) throw new AssertionError(message);
}

export function assertHas(object: any,
                          properties: (PropertyDeclaration | string)[]) {
  if (object === undefined) throw new AssertionError('object is undefined');
  for (const p of properties) {
    let property: PropertyDeclaration;
    if (typeof p === 'string') {
      property = {
        name: <string>p,
        type: null,
      };
    } else {
      property = <PropertyDeclaration>p;
    }

    const value: any = object[property.name];
    if (value === undefined) {
      throw new AssertionError('missing property "' + property.name +
                '" in object.');
    }
    if (property.type !== null && !isInstance(value, property.type)) {
      throw new AssertionError('property "' + property.name + '" has' +
            ' type "' + typeOf(value).name + '" but should be instance of' +
            ' "' + (<any>property.type).name) + '"';
    }
  }
}

export function assertInstance(value: any, type: any) {
  if (!isInstance(value, type)) {
    throw new AssertionError('value has' +
          ' type "' + typeOf(value).name + '" but should be instance of' +
          ' "' + type.name + '"');
  }
}

export function assertDefined(value: any) {
  if (value === undefined) throw new AssertionError('value is undefined');
}

export function ensure<T>(value: T | null | undefined): T {
  if (value != null) return <T>value;
  const typeFound = (value === null ? 'null' : 'undefined');
  throw new AssertionError(`unexpected ${typeFound}`);
}
