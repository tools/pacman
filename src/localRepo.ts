import { compareTuples } from './utils/compareTuples';
import { maxByWith, map, groupBy, entries } from './utils/functools';
import { Package, Dependency } from './interfaces';
import { native } from './native';
import { fileWriteSync } from './fs';
import { RuntimeError } from './errors';
import { getContainerPackage } from './containerVersion';
import * as _ from 'lodash';

export interface LocalRepo {
  /**
   * What the user want to have installed.
   *
   * Contains one dependency per game, without version constraints.
   *
   * Updating Psicool is done by means of satisfying this set of dependencies
   * with a new remote package list.
   */
  rootDependencies: Dependency[];

  /**
   * Complete list of all installed packages in the system, including obsolete
   * or unrunnable ones.
   */
  installedPackages: Package[];

  installedSizes: { [packageKey: string]: number };
}

/**
 * Reads the current local package database.
 */
export function readLocalRepo(): LocalRepo {
  const fileContents = native.fileReadSync('local.json');
  if (fileContents) {
    // File exists, parse it and return its contents.
    return <LocalRepo>JSON.parse(fileContents);
  }
  // File does not exist (new installation of Psicool).
  // Start with an empty local repo (nothing installed yet).
  return { rootDependencies: [], installedPackages: [], installedSizes: {} };
}

/**
 * Returns a list of the currently installed packages, INCLUDING the current
 * version of the container-sdk, which is not stored in local.json.
 */
export function getInstalledPackages(): Package[] {
  return [getContainerPackage()].concat(readLocalRepo().installedPackages);
}

/**
 * Overwrite the current local package database with the provided data.
 */
export function writeLocalRepo(repo: LocalRepo) {
  if (_.find(repo.installedPackages, p => p.role === 'container-sdk')) {
    throw new RuntimeError('container-sdk version must not be stored in local.json');
  }
  fileWriteSync('local.json', JSON.stringify(repo));
}

export function getInstalledGames(): Package[] {
  const versionsByPackageName = groupBy(
    readLocalRepo().installedPackages.filter(p => p.role === 'game'),
    (p => p.name),
  );

  // Return latest versions
  return map(entries(versionsByPackageName), ([packageName, versions]) =>
    maxByWith(versions, p => p.version, compareTuples)!);
}
