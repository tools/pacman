interface DownloadProgress {
  /** Total download size in bytes. */
  totalSize: number;

  /** Amount of bytes currently downloaded. */
  downloadedSize: number;
}

/**
 * These APIs are implemented in the wrapper apps (iOS and Android).
 */
export interface NativeBindings {
  /**
   * Performs an HTTP GET request to the specified URL and passes
   * the contents to a callback.
   */
  httpGetText(
    url: string,
    cb: (
      /* "network-error" if the server could not be reached,
       * null in other case. */
      err: 'network-error' | null,
      /* Contents of the response as a string, or null if the
       * server could not be reached. */
      text: string | null,
      /* HTTP status code returned by the server, or null if the
       * server could not be reached. */
      code: number | null,
    ) => void,
  ): void;

  /**
   * Reads a text file from the internal memory synchronously.
   * If the file does not exist, it returns null.
   *
   * The root directory of the paths specified must be a cache directory and
   * must be the same as used by `installZip()` so if the operating system
   * decides to wipe the application cache both the data and the indices are
   * deleted. Having one but not the other would be fatal as the package
   * manager would think a game is installed while it isn't, failing at
   * runtime, or -- in the other case, when the index has been wiped but the
   * games haven't -- the space taken may never be used or reclaimed.
   */
  fileReadSync(path: string): string | null;

  /**
   * Writes a text file from the internal memory synchronously.
   * Should create intermediary directories automatically.
   */
  fileWriteSync(path: string, contents: string): 'ok' | 'disk-full';

  /**
   * Downloads and uncompresses a zip package in a given directory.
   *
   * In order to minimize disk usage (and therefore the chances of it running
   * out, it should perform the decompression in streaming).
   *
   * The package manager will only call this function when there is enough
   * space for the uncompressed contents of the file according to the remote
   * repository index.
   *
   * Note: All Psicool zip packages have a top level directory that must be
   * stripped when uncompressing. The name of this top level directory always
   * matches the last part of the path. For instance, an archive may have the
   * following files:
   *
   *  zoobalance-1/psicool.json
   *  zoobalance-1/game-bundle.js
   *  zoobalance-1/...
   *
   * The value of the `path` parameter will be `game/zoobalance/zoobalance-1`
   * in that case, and the resulting files will have paths like these:
   *
   *  game/zoobalance/zoobalance-1/psicool.json
   *  game/zoobalance/zoobalance-1/game-bundle.js
   *  game/zoobalance/zoobalance-1/...
   *
   * @param url URL that will be downloaded
   * @param path Path where the zip will be uncompressed inside the
   * application directory.
   * @param cbProgress Will be called by the native application as the
   * download progresses.
   * @param cbCompletion Will be called by the native application when the
   * archive has been fully decompressed.
   */
  installZip(
    url: string,
    path: string,
    cbProgress: (p: DownloadProgress) => void,
    cbCompletion: (
      err:
        'network-error' | /* Could not reach the server */
        'http-error' | /* Server returned non-200 */
        'disk-full' | /* Disk full while uncompressing the file */
        null, /* Success */
      code: number | null, /* HTTP status code */
    ) => void,
  ): void;

  /**
   * Recursively delete directory.
   */
  recursiveDelete(path: string): void;

  /**
   * Get the amount of free space available in the cache directory where
   * games are uncompressed, in bytes.
   */
  getFreeSpace(): number;
}

/** Native bindings are loaded as window.pacmanNative. */
declare const pacmanNative: NativeBindings;

export const native = pacmanNative;
