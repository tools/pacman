import { Package } from './interfaces';
import { RuntimeError } from './errors';

let containerVersion: number[] | null = null;

export function setContainerVersion(version: number[]) {
  containerVersion = version;
}

export function getContainerPackage(): Package {
  if (containerVersion != null) {
    return {
      version: containerVersion,
      name: 'container-sdk',
      dependencies: [],
      role: 'container-sdk',
    };
  }
  throw new RuntimeError('no container version set yet!');
}
