/**
 * High level package manager API.
 *
 * Application code should only use the methods exported here.
 *
 * They all use the task queue to avoid synchronization issues.
 */

import { DownloadProgress, Package } from './interfaces';
import { enqueueTask, TaskPriority } from './taskQueue';
import { fetchRemotePackageList, RemotePackageList } from './packageCache';
import { installPackageByName } from './installPackage';
import { updateAllPackages } from './updatePackages';
import { removeGame } from './uninstallPackage';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import {
    startGame as _startGame,
    stopGame as _stopGame,
} from './packagesBeingUsed';
import { native } from './native';
import {
    setContainerVersion as _setContainerVersion,
    getContainerPackage,
} from './containerVersion';
import { httpGetJson } from './net';
import { getInstalledPackages } from './localRepo';
import { solveDependencies } from './solveDependencies';
import { AssertionError, ensure } from './utils/assert';
import { recursiveDelete } from './fs';
import { lruPushGameFront } from './lru';
import { setRepoUrl as _setRepoUrl } from './remoteRepo';
import { fetchInstallableGameList as _fetchInstallableGameList } from './fetchInstallableGameList';
import { HttpError } from './errors';

export { ConnectionError, HttpError, NotEnoughSpace } from './errors';
export { pacmanVersion } from './version';

export function setContainerVersion(version: number[]): void {
  _setContainerVersion(version);
}

/**
 * Used during development to set the repository URL to other than the default.
 */
export function setRepoUrl(url: string) {
  _setRepoUrl(url);
}

/**
 * Install the latest version of game and all its dependencies by its code name.
 *
 * The returned promise resolves when the process is complete, or may be
 * rejected with one of the following errors:
 *
 *  * `ConnectionError` if the server cannot be reached.
 *
 *  * `HttpError` if there is a working network connection but the server
 *    returns an invalid response.
 *
 *  * `NotEnoughSpace` if there is near to zero free space and the game cannot
 *    be installed in any way even after removing all the other games from
 *    memory.
 */
export function installGame(
  gameName: string,
  cbProgress?: (p: DownloadProgress) => void,
): Promise<Package> {
  return enqueueTask(
    () => new Promise<Package>((
      resolve: (p: Promise<Package>) => void,
      reject: (e: Error) => void,
    ) => {
      fetchRemotePackageList()
        .then((remotePackageList: RemotePackageList) => {
          resolve(installPackageByName(gameName, remotePackageList, cbProgress));
          return null;
        })
        .catch((e: Error) => {
          if (isGameInstalled(gameName)) {
            resolve(Promise.resolve<Package>(<Package>getInstalledPackages()
              .find((p:Package) => p.name === gameName)));
          } else reject(e);
        });
    }),
    TaskPriority.Normal,
  );
}

/**
 * Updates all the games and libraries installed.
 *
 * The returned promise resolves when the process is complete, or may be
 * rejected with one of the following errors:
 *
 *  * `ConnectionError` if the server cannot be reached.
 *
 *  * `HttpError` if there is a working network connection but the server
 *    returns an invalid response.
 *
 *  * `NotEnoughSpace` if a game could not be updated due to not having enough
 *    memory. Least recently played games are deleted before reaching this case,
 *    so this will only be thrown if the space is really low.
 */
export function updatePlatform(): Promise<void> {
  return updateAllPackages();
}

/**
 * Uninstall a game on demand. Probably this won't be exposed to users, but
 * it's exported just in case;
 *
 * The returned promise resolves when the process is complete.
 */
export function uninstallGame(gameName: string): Promise<void> {
  return enqueueTask(
    () => {
      removeGame(gameName, []);
      return Promise.resolve(); // sync!
    },
    TaskPriority.Normal,
  );
}

export function fetchInstallableGameList(): Promise<string[]> {
  return _fetchInstallableGameList();
}

/**
 * Checks whether a certain game is currently installed.
 */
export function isGameInstalled(gameName: string): boolean {
  const dependencies = solveDependencies(
    [{ name: gameName, minVersion: null, maxVersion: null }],
    getInstalledPackages(),
  );

  return dependencies != null;
}

/**
 * Returns the set of packages needed to run a game. Includes both the game
 * itself, as updated as possible and all its required SDK components.
 *
 * May return null if the game cannot be run due to unsatisfied dependencies:
 * for instance, the container app may have been just updated to a new version
 * incompatible with previous games. Until a platform update is complete, they
 * cannot be run.
 *
 * Calling this function also ensures that the game is not deleted from the
 * disk at least until stopGame() is called.
 */
export function startGame(gameName: string): Package[] | null {
  lruPushGameFront(gameName);
  return _startGame(gameName);
}

/**
 * Should be called when a game is closed in order to reclaim any resources
 * that could not be wiped during an update but were not because of the
 * game that was currently running.
 */
export function stopGame(): void {
  _stopGame();
}

/**
 * Given a package name resolves its dependencies from the repository and
 * returns them without touching the file system.
 *
 * This function is used to load games directly through http:// during
 * development instead of installing them each time.
 */
export function selectDependenciesFromRepo(gameName: string): Promise<Package[] | null> {
  return fetchRemotePackageList()
    .then((remotePackageList: RemotePackageList) =>
      solveDependencies(
        [{ name: gameName, minVersion: null, maxVersion: null }],
        [getContainerPackage()].concat(remotePackageList.packages),
      ));
}


/*
 * Functions exported in order for developers to test their native bindings.
 */

/**
 * Deletes the entire cache used by the package manager. Useful for development.
 */
export function deleteAllFiles(): void {
  recursiveDelete('game');
  recursiveDelete('framework');
  recursiveDelete('client-sdk');
  recursiveDelete('local.json');
  recursiveDelete('lru.json');
}

export function printError(e: Error) {
  console.error(e.message);
  console.error(e.stack);
}

export function test1_setTimeout() {
  setTimeout(() => console.log('two seconds later'), 2000);
}

export function test2_httpGetText() {
  setTimeout(
    () => {
      try {
        native.httpGetText('http://ip.jsontest.com/', (err, text, code) => {
          console.log('callback called');
          console.log(err);
          console.log(text);
          console.log(code);
        });
      } catch (e) {
        console.log('exception thrown');
        console.log(e.message);
        console.log(e.stack);
      }
    },
    0,
  );
}

export function test3_httpPromise() {
  return httpGetJson('http://ip.jsontest.com/')
    .then((d: any) => console.log(JSON.stringify(d)))
    .catch(printError);
}

export function test4_files() {
  native.recursiveDelete('patata.txt');

  console.log(`free space: ${native.getFreeSpace()}`);
  console.log(native.fileReadSync('patata.txt'));
  native.fileWriteSync('patata.txt', 'me gustan las patatas fritas.');
  console.log(`free space: ${native.getFreeSpace()}`);
  console.log(native.fileReadSync('patata.txt'));
}

export function test5_directories() {
  native.fileWriteSync('a/a.txt', 'miau');
  native.fileWriteSync('a/b.txt', 'guau');
  console.log(native.fileReadSync('a/a.txt'));
  console.log(native.fileReadSync('a/b.txt'));
  native.recursiveDelete('a');
  console.log(native.fileReadSync('a/a.txt'));
  console.log(native.fileReadSync('a/b.txt'));
}

export function test6_installGame() {
  installGame('zoo-balance', (p) => {
    console.log(`progress ${p.downloadedSize}/${p.totalSize} (${(
      100 * p.downloadedSize / p.totalSize).toFixed(0)})`);
  }).then((pkg: Package) => {
    console.log(JSON.stringify(pkg));

    const deps = solveDependencies(
      [{ name: 'zoo-balance', minVersion: null, maxVersion: null }],
      getInstalledPackages(),
    );
    if (deps == null) throw new AssertionError('could not solve dependencies after installation!');

    const framework = ensure(_.find(deps, (d: Package) => d.role === 'framework'));
    console.log(native.fileReadSync(`game/zoobalance/zoobalance-${
      pkg.version.join('.')}/psicool.json`));
    console.log(native.fileReadSync(`framework/cocos2d/cocos2d-${
      framework.version.join('.')}/psicool.json`));
  }).catch((e: Error) => {
    console.log(e.message);
    console.log(e.stack);
  });
}

export function testThrowErrorSubclass() {
  throw new HttpError(404, 'not found');
}

export function testThrowErrorSubclassAsync() {
  return Promise.resolve()
    .then(() => {throw new HttpError(404, 'not found');});
}

function delay(t: number):Promise<void> {
  return new Promise<void>((resolve: Function) => {
    setTimeout(resolve, t);
  });
}

export function testClock(i = 0) {
  console.log(i);
  delay(1000).then(() => {
    testClock(i + 1);
  });
}
