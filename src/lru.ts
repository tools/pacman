import { native } from './native';
import { Package, packageKey } from './interfaces';
import { RemotePackageList, getPackageInstalledSize } from './packageCache';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import { ensure } from './utils/assert';
import { NotEnoughSpace } from './errors';
import { removeGameIfStillExists } from './uninstallPackage';
import { fileWriteSync } from './fs';
import { readLocalRepo } from './localRepo';

/**
 * Returns a list of the played games sorted by how recently they were played.
 *
 * Most recently played games are at the start of the list.
 */
export function readLruList(): string[] {
  return JSON.parse(native.fileReadSync('lru.json') || '[]');
}

/**
 * Must be called when a game is launched to move it to the front of the LRU
 * list.
 *
 * Least recently played games are deleted first when disk space becomes scarce.
 */
export function lruPushGameFront(gameName: string): void {
  const oldLRU = readLruList();
  const newLRU = [gameName].concat(oldLRU.filter(x => x !== gameName));
  fileWriteSync('lru.json', JSON.stringify(newLRU));
}

/**
 * Reads the least recently played game and pops it out from the LRU list.
 *
 * It will return null if the LRU list is empty.
 */
export function lruPopGame(): string | null {
  const lru = readLruList();
  const poppedGame = lru.pop();

  if (poppedGame) {
    fileWriteSync('lru.json', JSON.stringify(lru));
    return poppedGame;
  }
  return null;
}

export const maxCacheSize = 128 * 1024 * 1024; // 128 MiB

function getInstalledPackagesTotalSize(): number {
  const repo = readLocalRepo();
  return _.sum(_.map(repo.installedPackages, p =>
    ensure(repo.installedSizes[packageKey(p)])));
}

/**
 * Uninstall least recently used games until there is enough free space to
 * install the requested packages.
 */
export function freeSpaceForPackages(
  packages: Package[],
  doNotUninstallTheseDependencies: Package[],
  remotePackageList: RemotePackageList)
: Promise<void> {
  const neededSpace = _.sum(_.map(packages, p =>
    getPackageInstalledSize(p, remotePackageList)));

  while (true) {
    const currentFreeSpace = Math.min(
      native.getFreeSpace(),
      maxCacheSize - getInstalledPackagesTotalSize(),
    );

    if (neededSpace > currentFreeSpace) {
      const gameToUninstall = lruPopGame();
      if (gameToUninstall) {
        // uninstall the game, but do not remove any dependencies of the
        // game we're trying to install now.
        removeGameIfStillExists(gameToUninstall, doNotUninstallTheseDependencies);
      } else {
        // No space at all, even after uninstalling every single game!
        throw new NotEnoughSpace();
      }
    } else {
      // Done, we finally have enough space
      return Promise.resolve();
    }
  }
}
