import { readLocalRepo, writeLocalRepo } from './localRepo';
import * as _ from 'lodash';
import { cleanRepo } from './cleanRepo';
import { RuntimeError } from './errors';
import { Package } from './interfaces';

export function removeGameIfStillExists(
  packageName: string,
  butKeepThesePackages: Package[] = [],
): void {
  const localRepo = readLocalRepo();

  if (_.find(localRepo.rootDependencies, d => d.name === packageName)) {
    // It exists, delete it.
    removeGame(packageName, butKeepThesePackages);
  }
}

export function removeGame(packageName: string, butKeepThesePackages: Package[] = []): void {
  const localRepo = readLocalRepo();

  // Remove the now unwanted dependency
  if (!_.find(localRepo.rootDependencies, d => d.name === packageName)) {
    throw new RuntimeError(
      `could not remove the requested package because it was not found in rootDependencies: ${
        packageName}`);
  }

  const rootDependencies = _.filter(localRepo.rootDependencies, d => d.name !== packageName);

  // Update the local repo, removing the game from root dependencies
  writeLocalRepo({
    rootDependencies,
    installedPackages: localRepo.installedPackages,
    installedSizes: localRepo.installedSizes,
  });

  // Really delete stuff
  cleanRepo(butKeepThesePackages);
}
