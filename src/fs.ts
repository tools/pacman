/** Slim wrapper over native file system methods that uses exceptions. */

import { native } from './native';
import { NotEnoughSpace } from './errors';
/**
 * Reads a text file from the internal memory synchronously.
 * If the file does not exist, it returns null.
 */
export function fileReadSync(path: string): string | null {
  return native.fileReadSync(path);
}

/**
 * Writes a text file from the internal memory synchronously.
 * Should create intermediary directories automatically.
 */
export function fileWriteSync(path: string, contents: string): void {
  const ret = native.fileWriteSync(path, contents);
  if (ret === 'disk-full') {
    throw new NotEnoughSpace();
  }
}

/**
 * Recursively delete directory.
 */
export function recursiveDelete(path: string): void {
  return native.recursiveDelete(path);
}
