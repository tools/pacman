/**
 * Thrown by some programming errors.
 */
export class RuntimeError extends Error {
  constructor(message: string = 'runtime error') {
    super(message);
    this.name = 'RuntimeError';
    if (typeof (<any>Error).captureStackTrace !== 'undefined') {
      (<any>Error).captureStackTrace(this, RuntimeError);
    }

    // Due to limitations of the ES5 downlevel compiler we need to
    // set the prototype explicitly:
    // tslint:disable-next-line:max-line-length
    // https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
    (<any>this).__proto__ = RuntimeError.prototype;
  }
}

/**
 * Thrown when the server cannot be reached.
 */
export class ConnectionError extends Error {
  constructor() {
    super('connection error');
    this.name = 'ConnectionError';
    if (typeof (<any>Error).captureStackTrace !== 'undefined') {
      (<any>Error).captureStackTrace(this, ConnectionError);
    }

    (<any>this).__proto__ = ConnectionError.prototype;
  }
}

/**
 * Thrown when the server returns a non 20x response.
 */
export class HttpError extends Error {
  code: number;

  constructor(code: number, message: string) {
    super(message);
    this.name = 'HttpError';
    this.code = code;
    if (typeof (<any>Error).captureStackTrace !== 'undefined') {
      (<any>Error).captureStackTrace(this, HttpError);
    }

    (<any>this).__proto__ = HttpError.prototype;
  }
}

/**
 * Thrown during installation and update operations when
 * there is not enough disk space to satisfy the request.
 */
export class NotEnoughSpace extends Error {
  constructor() {
    super('not enough space');
    this.name = 'NotEnoughSpace';
    if (typeof (<any>Error).captureStackTrace !== 'undefined') {
      (<any>Error).captureStackTrace(this, NotEnoughSpace);
    }

    (<any>this).__proto__ = NotEnoughSpace.prototype;
  }
}
