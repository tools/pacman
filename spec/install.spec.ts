import SinonSpy = Sinon.SinonSpy;

const testListJson1 = `
{
  "packages": [
    {
      "name": "zoobalance",
      "role": "game",
      "version": [1],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "cocos2d",
      "role": "framework",
      "version": [3, 11, 1],
      "dependencies": []
    },
    {
      "name": "client-sdk",
      "role": "client-sdk",
      "version": [0, 0, 0],
      "dependencies": [
        {"name": "container-sdk", "minVersion": [0, 0], "maxVersion": [2, 0]},
        {"name": "cocos2d", "minVersion": [3, 11, 1], "maxVersion": [3, 11, 2]}
      ]
    }
  ],
  "packageSizes": {
    "zoobalance-1": {"download": 1939057, "disk": 2547712},
    "cocos2d-3.11.1": {"download": 359864, "disk": 1572864},
    "client-sdk-0.0.0": {"download": 19259, "disk": 98304}
  }
}
`;

/** Like testListJson1... with updates! */
const testListJson2 = `
{
  "packages": [
    {
      "name": "zoobalance",
      "role": "game",
      "version": [2],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "zoobalance",
      "role": "game",
      "version": [1],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "cocos2d",
      "role": "framework",
      "version": [3, 11, 1],
      "dependencies": []
    },
    {
      "name": "client-sdk",
      "role": "client-sdk",
      "version": [0, 0, 0],
      "dependencies": [
        {"name": "container-sdk", "minVersion": [0, 0], "maxVersion": [2, 0]},
        {"name": "cocos2d", "minVersion": [3, 11, 1], "maxVersion": [3, 11, 2]}
      ]
    },
    {
      "name": "client-sdk",
      "role": "client-sdk",
      "version": [0, 0, 1],
      "dependencies": [
        {"name": "container-sdk", "minVersion": [0, 0], "maxVersion": [2, 0]},
        {"name": "cocos2d", "minVersion": [3, 11, 1], "maxVersion": [3, 11, 2]}
      ]
    },
    {
      "name": "client-sdk",
      "role": "client-sdk",
      "version": [0, 1, 0],
      "dependencies": [
        {"name": "container-sdk", "minVersion": [0, 0], "maxVersion": [2, 0]},
        {"name": "cocos2d", "minVersion": [3, 11, 1], "maxVersion": [3, 11, 2]}
      ]
    }
  ],
  "packageSizes": {
    "zoobalance-2": {"download": 1939057, "disk": 2547712},
    "zoobalance-1": {"download": 1939057, "disk": 2547712},
    "cocos2d-3.11.1": {"download": 359864, "disk": 1572864},
    "client-sdk-0.0.0": {"download": 19259, "disk": 98304},
    "client-sdk-0.0.1": {"download": 19259, "disk": 98304},
    "client-sdk-0.1.0": {"download": 19259, "disk": 98304}
  }
}
`;

/** Contains three games of 14 MB each, so two of them can be installed at a time. */
const testListJsonLru = `
{
  "packages": [
    {
      "name": "fat-game-a",
      "role": "game",
      "version": [1],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "fat-game-b",
      "role": "game",
      "version": [1],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "fat-game-c",
      "role": "game",
      "version": [1],
      "dependencies": [
        {"name": "client-sdk", "minVersion": [0, 0], "maxVersion": [1, 0]}
      ]
    },
    {
      "name": "cocos2d",
      "role": "framework",
      "version": [3, 11, 1],
      "dependencies": []
    },
    {
      "name": "client-sdk",
      "role": "client-sdk",
      "version": [0, 0, 0],
      "dependencies": [
        {"name": "container-sdk", "minVersion": [0, 0], "maxVersion": [2, 0]},
        {"name": "cocos2d", "minVersion": [3, 11, 1], "maxVersion": [3, 11, 2]}
      ]
    }
  ],
  "packageSizes": {
    "fat-game-a-1": {"download": 1939057, "disk": 14000000},
    "fat-game-b-1": {"download": 1939057, "disk": 14000000},
    "fat-game-c-1": {"download": 1939057, "disk": 14000000},
    "cocos2d-3.11.1": {"download": 359864, "disk": 1572864},
    "client-sdk-0.0.0": {"download": 19259, "disk": 98304}
  }
}
`;

class MockBindings implements NativeBindings {
  filesystem: {[path: string]: string} = {};
  testList: string = testListJson1;
  brokenNetwork = false;
  diskFull = false;

  reset() {
    this.filesystem = {};
    this.testList = testListJson1;
    this.brokenNetwork = false;
    this.diskFull = false;
  }

  httpGetText(url: string, cb: (
    err: 'network-error' | null,
    text: string | null,
    code: number | null,
  ) => void): void {
    if (url === repoUrl + '/list.json') {
      setTimeout(
        () => {
          if (!this.brokenNetwork) {
            cb(null, this.testList, 200);
          } else {
            cb('network-error', null, null);
          }
        },
        0,
      );
    } else {
      throw new Error('unexpected request');
    }
  }

  fileReadSync(path: string): string|any {
    return this.filesystem[path];
  }

  fileWriteSync(path: string, contents: string): 'ok'|'disk-full' {
    if (!this.diskFull) {
      this.filesystem[path] = contents;
      return 'ok';
    }
    return 'disk-full';
  }

  installZip(
    url: string,
    path: string,
    cbProgress: (p: DownloadProgress) => void,
    cbCompletion: (
      err: 'network-error' | 'disk-full' | null,
      nullcode: number | null,
    ) => void,
  ): void {
    setTimeout(
      () => {
        if (!this.brokenNetwork) {
          if (!this.diskFull) {
            cbCompletion(null, null);
          } else {
            cbCompletion('disk-full', null);
          }
        } else {
          cbCompletion('network-error', null);
        }
      },
      0,
    );
  }

  recursiveDelete(path: string): void {
    const keys: string[] = _.keys(this.filesystem);
    for (const key of keys) {
      if (key.indexOf(path) === 0) {
        delete this.filesystem[key];
      }
    }
  }

  getFreeSpace(): number {
    // 100 MiB
    return 100 * 1024 * 1024;
  }
}
const mockBindings = new MockBindings();

declare const global: any;
global.pacmanNative = mockBindings;

import { getInstalledPackages, readLocalRepo } from '../src/localRepo';
import * as Promise from 'bluebird';
import { enqueueTask, TaskPriority } from '../src/taskQueue';
import { Package, packageKey, DownloadProgress } from '../src/interfaces';
import * as _ from 'lodash';
import { NativeBindings } from '../src/native';
import { repoUrl } from '../src/remoteRepo';
import {
    installGame, setContainerVersion,
    uninstallGame, updatePlatform, startGame, stopGame,
    ConnectionError, NotEnoughSpace,
} from '../src/highLevel';
import { cleanRepo } from '../src/cleanRepo';
import { fetchRemotePackageList } from '../src/packageCache';
import { solveDependencies } from '../src/solveDependencies';
import { getContainerPackage } from '../src/containerVersion';


setContainerVersion([0, 0, 0]);

describe('scheduler', () => {
  it('sets tasks', () =>
    Promise.all(_.map([1, 2, 3], num => enqueueTask(
      () => Promise.resolve(num),
      TaskPriority.Normal,
    )))
      .then((nums) => {
        expect(nums).to.deep.equal([1, 2, 3]);
      }));
});

describe('cleanRepo on empty state', () => {
  it('ends in a flash!', () => {
    cleanRepo();
  });
});

describe('fetchRemotePackageList()', () => {
  it('works', () =>
    fetchRemotePackageList()
      .then((ret) => {
        expect(ret.packages).not.to.be.undefined;
      }));
});

describe('dependency solving', () => {
  it('works', () =>
    fetchRemotePackageList()
      .then((remotePackageList) => {
        // Choose the best package and dependencies from the remote repository
        const packagesNeeded = solveDependencies(
          // any version of the requested package (preferably the most recent one)
          [{ name: 'zoobalance', minVersion: null, maxVersion: null }],
          [getContainerPackage()].concat(remotePackageList.packages),
        );
        console.log(packagesNeeded);
        expect(packagesNeeded).not.to.be.null;
        console.log(packagesNeeded!.map(p => p.role));
        expect(packagesNeeded!.length).to.equal(4);
      }));
});

import * as Sinon from 'sinon';
import { expect } from 'chai';
import noop = require('lodash/noop');

describe('installGame', () => {
  it('is capable of installing a game', () => {
    const progressSpy = Sinon.spy();
    Sinon.spy(mockBindings, 'installZip');

    return Promise.resolve()
    .then(() =>
    installGame('zoobalance', progressSpy))
    .then(() => {
      expect(progressSpy.called).to.be.true;
      // console.log(progressSpy..all()[0].args);

      expect((<Sinon.SinonSpy>mockBindings.installZip).calledThrice).to.be.true;
      // console.log((<Sinon.SinonSpy>mockBindings.installZip).
      //   .calls.allArgs().map(l => l[0].slice(25)));

      expect(readLocalRepo().rootDependencies.length).to.equal(1);
      console.log(getInstalledPackages());
      expect(getInstalledPackages().length).to.equal(4);
    });
  });

  it('the game is not nuked after cleanRepo', () => {
    return Promise.resolve()
      .then(() => {
        // check the game is not nuked after cleanRepo()
        cleanRepo();
        expect(readLocalRepo().rootDependencies.length).to.equal(1);
        expect(getInstalledPackages().length).to.equal(4);
      });
  });

  it('uninstall and it\'s gone', () => {
    return Promise.resolve()
      .then(() => {
        // uninstall and it's gone
        uninstallGame('zoobalance');
        expect(readLocalRepo().rootDependencies.length).to.equal(0);
        expect(getInstalledPackages().length).to.equal(1);
      });
  });

  it('reinstall it for the next test', () => {
    return installGame('zoobalance');
  });
});

function expectPackage(packages: Package[], key: string) {
  expect(_.find(packages, p => packageKey(p) === key), `Expected ${key}`).not.to.be.undefined;
}

function expectNotPackage(packages: Package[], key: string) {
  expect(_.find(packages, p => packageKey(p) === key), `Unexpected ${key}`).to.be.undefined;
}

describe('updatePlatform()', () => {
  it('updates both games and libraries', () => {
    return Promise.resolve()
      .then(() => {
        mockBindings.reset();
        mockBindings.testList = testListJson1;
      })
      .then(() => installGame('zoobalance'))
      .then(() => {
        mockBindings.testList = testListJson2;
      })
      .then(() => updatePlatform())
      .then(() => {
        const packages = getInstalledPackages();
        expect(packages.length).to.be.greaterThan(1);
        expect(packages.length).to.equal(4);

        // Updates are installed
        expectPackage(packages, 'zoobalance-2');
        expectPackage(packages, 'client-sdk-0.1.0');

        // Dependencies which had no updates remain installed too
        expectPackage(packages, 'cocos2d-3.11.1');

        // Old packages have been removed
        expectNotPackage(packages, 'zoobalance-1');
        expectNotPackage(packages, 'client-sdk-0.0.0');

        // Older but still more recent packages have not been installed
        expectNotPackage(packages, 'client-sdk-0.0.1');
      });
  });
});

function simulateUserPlaysGame(gameName: string) {
  const packages = startGame(gameName)!;
  expect(packages, 'startGame() failed').not.to.be.null;
  expect(packages.length).to.equal(4);
  expect(_.find(packages, p => p.name === gameName)).not.to.be.undefined;

  stopGame();
}

describe('LRU', () => {
  it('uninstall older games to fit new ones', () => {
    return Promise.resolve()
      .then(() => {
        mockBindings.reset();
        mockBindings.testList = testListJsonLru;
      })
      .then(() => installGame('fat-game-a', noop))
      .then(() => simulateUserPlaysGame('fat-game-a'))
      .then(() => installGame('fat-game-b', noop))
      .then(() => simulateUserPlaysGame('fat-game-b'))
      .then(() => installGame('fat-game-c', noop))
      .then(() => simulateUserPlaysGame('fat-game-c'))
      .then(() => {
        const packages = getInstalledPackages();
        expectPackage(packages, 'fat-game-b-1');
        expectPackage(packages, 'fat-game-c-1');
        expectNotPackage(packages, 'fat-game-a-1');
      });
  });
});

describe('packagesBeingUsed', () => {
  it('prevents games being played from been erased while they are updated', () => {
    return Promise.resolve()
      .then(() => {
        mockBindings.reset();
        mockBindings.testList = testListJson1;
      })
      .then(() => installGame('zoobalance', noop))
      .then(() => {
        expect(startGame('zoobalance')).to.be.ok;
      })
      .then(() => {
        mockBindings.testList = testListJson2;
      })
      .then(() => updatePlatform())
      .then(() => {
        const packages = getInstalledPackages();
        // The update has been installed
        expectPackage(packages, 'zoobalance-2');
        // The previous version is still alive
        expectPackage(packages, 'zoobalance-1');

        // After the game ends, it's cleared
        stopGame();
        expectNotPackage(getInstalledPackages(), 'zoobalance-1');
      });
  });
});

describe('network errors', () => {
  it('are propagated from native methods', () => {
    mockBindings.reset();
    mockBindings.testList = testListJson1;
    mockBindings.brokenNetwork = true;

    let failed = false;

    return installGame('zoobalance', noop)
      .then(() => { throw new Error('must not suceed!'); })
      .catch(ConnectionError, () => {
        failed = true;
      })
      .finally(() => {
        expect(failed).to.be.ok;
      });
  });
});


describe('disk errors', () => {
  it('are propagated from native methods', () => {
    mockBindings.reset();
    mockBindings.testList = testListJson1;
    mockBindings.diskFull = true;

    let failed = false;

    return installGame('zoobalance', noop)
      .then(() => {throw new Error('must not suceed!');})
      .catch(NotEnoughSpace, () => {
        failed = true;
      })
      .finally(() => {
        expect(failed).to.be.ok;
      });
  });
});
