import { compareTuples } from '../src/utils/compareTuples';
import {
  versionInRange,
  moreRecentPackagesFirst,
  solveDependencies,
} from '../src/solveDependencies';
import { Package } from '../src/interfaces';
import * as _ from 'lodash';
import { expect } from 'chai';

describe('compareTuples', () => {
  it('works with same sizes', () => {
    expect(compareTuples([1, 0], [0, 0])).to.equal(1);
    expect(compareTuples([0, 0], [0, 0])).to.equal(0);
    expect(compareTuples([-1, 0], [0, 0])).to.equal(-1);
    expect(compareTuples([0, 1], [0, 0])).to.equal(1);
    expect(compareTuples([0, 0], [0, 1])).to.equal(-1);
  });

  it('works with different sizes', () => {
    expect(compareTuples([1, 5], [1, 5, 0])).to.equal(-1);
    expect(compareTuples([1, 5, 0], [1, 5])).to.equal(1);
    expect(compareTuples([1, 5, 0], [1, 6])).to.equal(-1);
    expect(compareTuples([1, 6], [1, 5, 0])).to.equal(1);
  });
});

describe('versionInRange', () => {
  it('works as expected', () => {
    expect(versionInRange([1, 5, 1], [1, 0], [2, 0])).to.be.true;
    expect(versionInRange([1, 5, 1], [1, 0], [1, 5])).to.be.false;
    expect(versionInRange([1, 5, 1], [1, 5], [1, 6])).to.be.true;
    expect(versionInRange([1, 5, 1], [1, 5, 1], [1, 6])).to.be.true;
    expect(versionInRange([1, 5, 1], [1, 5, 2], [1, 6])).to.be.false;
  });
});

describe('package sorting', () => {
  it('works as expected', () => {
    expect(true).to.be.true;
    const packages: Package[] = [{
      role: 'game',
      name: 'zoobalance',
      version: [1, 0],
      dependencies: [],
    }, {
      role: 'game',
      name: 'zoobalance',
      version: [1, 1],
      dependencies: [],
    }, {
      role: 'game',
      name: 'zoobalance',
      version: [2, 0],
      dependencies: [],
    }];

    const versionsSorted = packages
      .sort(moreRecentPackagesFirst)
      .map(p => p.version);

    expect(versionsSorted[0]).to.deep.equal([2, 0]);
    expect(versionsSorted[1]).to.deep.equal([1, 1]);
    expect(versionsSorted[2]).to.deep.equal([1, 0]);
  });
});

describe('package resolution testcase', () => {
  const localPackageList: Package[] = [
    { role:'container-sdk', name: 'container-sdk', version: [2, 0, 0], dependencies: [] },

    { role:'framework', name: 'cocos2d', version: [3, 11, 1], dependencies: [] },
    { role:'framework', name: 'cocos2d', version: [3, 11, 2], dependencies: [] },

    { role:'client-sdk', name: 'client-sdk', version: [1, 0, 0], dependencies: [
      { name: 'container-sdk', minVersion: [1, 0], maxVersion: [2, 0] },
      { name: 'cocos2d', minVersion: [3, 11, 1], maxVersion: [3, 11, 2] },
    ] },
    { role:'client-sdk', name: 'client-sdk', version: [1, 1, 0], dependencies: [
      { name: 'container-sdk', minVersion: [1, 0], maxVersion: [2, 0] },
      { name: 'cocos2d', minVersion: [3, 11, 2], maxVersion: [3, 11, 3] },
    ] },
    { role:'client-sdk', name: 'client-sdk', version: [2, 0, 0], dependencies: [
      { name: 'container-sdk', minVersion: [2, 0], maxVersion: [3, 0] },
      { name: 'cocos2d', minVersion: [3, 11, 2], maxVersion: [3, 11, 3] },
    ] },

    { role:'game', name: 'zoobalance', version: [1], dependencies: [
      { name: 'client-sdk', minVersion: [1, 0], maxVersion: [2, 0] },
    ] },
    { role:'game', name: 'zoobalance', version: [2], dependencies: [
      { name: 'client-sdk', minVersion: [1, 0], maxVersion: [2, 0] },
    ] },
    { role:'game', name: 'calculator_challenge', version: [1], dependencies: [
      { name: 'client-sdk', minVersion: [2, 0], maxVersion: [3, 0] },
    ] },
  ];

  it('calculator_challenge can be run', () => {
    const deps = solveDependencies(
      _.find(localPackageList, p => p.name === 'calculator_challenge')!.dependencies,
      localPackageList);
    expect(deps).not.to.be.null;
    expect(deps!.length).to.equal(3);
    expect(_.find(deps!, p => p.role === 'client-sdk')!.version).to.deep.equal([2, 0, 0]);
    expect(_.find(deps!, p => p.role === 'framework')!.version).to.deep.equal([3, 11, 2]);
  });

  it('zoobalance cannot be run (insuficient container version)', () => {
    const deps = solveDependencies(
      _.find(localPackageList, p => p.name === 'zoobalance')!.dependencies,
      localPackageList);
    expect(deps).to.be.null;
  });

  it('zoobalance can be run with a newer client-sdk compatible with the new container', () => {
    localPackageList.push(
      { role:'client-sdk', name: 'client-sdk', version: [1, 3, 0], dependencies: [
        { name: 'container-sdk', minVersion: [2, 0], maxVersion: [3, 0] },
        { name: 'cocos2d', minVersion: [3, 11, 2], maxVersion: [3, 11, 3] },
      ] });

    const deps = solveDependencies(
      _.find(localPackageList, p => p.name === 'zoobalance')!.dependencies,
      localPackageList);
    expect(deps).not.to.be.null;
    expect(deps!.length).to.equal(3);
    expect(_.find(deps!, p => p.role === 'client-sdk')!.version).to.deep.equal([1, 3, 0]);
    expect(_.find(deps!, p => p.role === 'framework')!.version).to.deep.equal([3, 11, 2]);
  });
});
