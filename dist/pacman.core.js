/*
 * psicool-pacman@2.2.3
 *
 *   psicool package manager
 *
 *   
 */
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.pacmanCore = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var compareTuples_1 = require("./utils/compareTuples");
function find(list, predicate) {
    for (var i = 0; i < list.length; i += 1) {
        if (predicate(list[i])) {
            return list[i];
        }
    }
}
function versionInRange(supplied, min, max) {
    // min <= supplied < max
    return (min == null || compareTuples_1.compareTuples(min, supplied) <= 0) &&
        (max == null || compareTuples_1.compareTuples(supplied, max) < 0);
}
exports.versionInRange = versionInRange;
function moreRecentPackagesFirst(a, b) {
    return compareTuples_1.compareTuples(b.version, a.version);
}
exports.moreRecentPackagesFirst = moreRecentPackagesFirst;
/**
 * The local repository may have several options for a certain
 * dependency of a package. Only one package must be used for
 * each game dependency, and they must be selected every time
 * the game is launched.
 *
 * This function receives a package (e.g. a game) and returns a
 * list of chosen dependencies that match its requirements
 * and are as updated as possible.
 *
 * This function may also be used on remote package lists to
 * resolve package dependencies on install time.
 */
function solveDependencies(dependencies, packageList) {
    return recursiveSolve(dependencies, []);
    /**
     * Backtracking algorithm to find packages that satisfy some dependencies
     * without conflicts with already selected packages.
     *
     * @param dependencies Dependencies of the package to install.
     * @param currentSolution List of packages already selected. Only one
     * version per package may become part of the solution.
     */
    function recursiveSolve(dependencies, currentSolution) {
        if (dependencies.length === 0) {
            // No more dependencies to solve for this package.
            return currentSolution;
        }
        // Solve first dependency.
        var dep = dependencies[0];
        // Is there a package already providing it in the current solution?
        var currentPackage = find(currentSolution, function (p) { return p.name === dep.name; });
        // If there is such a package but its version conflicts, this is not a valid solution.
        if (currentPackage && !versionInRange(currentPackage.version, dep.minVersion, dep.maxVersion)) {
            return null;
        }
        var versionsFulfilling = 
        // If there is already a package selected for this dependency name, we can't
        // choose another.
        currentPackage != null ? [currentPackage]
            // If there is currently not such a package in the solution, let's find one.
            // Try with the most updated packages first.
            : packageList
                .filter(function (p) { return p.name === dep.name &&
                versionInRange(p.version, dep.minVersion, dep.maxVersion); })
                .sort(moreRecentPackagesFirst);
        // Try each of the versions found until one is a solution
        for (var _i = 0, versionsFulfilling_1 = versionsFulfilling; _i < versionsFulfilling_1.length; _i++) {
            var candidateDependency = versionsFulfilling_1[_i];
            // Solve nested dependencies of the selected package
            var newPartialSolution = recursiveSolve(candidateDependency.dependencies, currentSolution.concat([candidateDependency]));
            // This dependency can be satisfied with this version. All its nested
            // dependencies match. They're now in `newPartialSolution`.
            if (newPartialSolution != null) {
                // Solve the rest of dependencies of `package`.
                var solution = recursiveSolve(dependencies.slice(1), newPartialSolution);
                if (solution != null) {
                    return solution;
                }
            }
            // Else keep trying with other version of the package.
        }
        // Tried every version. No solution.
        return null;
    }
}
exports.solveDependencies = solveDependencies;

},{"./utils/compareTuples":2}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Compares tuples item by item, in the same way Python does it.
 */
function compareTuples(a, b) {
    if (a.length === 0 && b.length === 0)
        return 0; // Emptied both tuples, they are equal.
    if (a.length > 0 && b.length === 0)
        return 1; // Emptied b, a still has elements. a is greater.
    if (a.length === 0 && b.length > 0)
        return -1; // Emptied a, b still has elements. a is lesser.
    if (a[0] > b[0])
        return 1; // a is greater
    if (a[0] < b[0])
        return -1; // a is lesser
    return compareTuples(a.slice(1), b.slice(1)); // Compare rest of the tuple
}
exports.compareTuples = compareTuples;

},{}]},{},[1])(1)
});
